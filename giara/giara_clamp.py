from gi.repository import Handy


class GiaraClamp(Handy.Clamp):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_maximum_size(1200)
        self.set_tightening_threshold(1000)
        self.set_margin_start(12)
        self.set_margin_end(12)
        self.set_margin_top(12)
        self.set_margin_bottom(12)
