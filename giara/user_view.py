from gi.repository import Gtk
from giara.single_post_stream_view import SinglePostStreamView
from giara.user_heading import UserHeading


class UserView(SinglePostStreamView):
    def __init__(self, user, show_post_func):
        self.user = user
        super().__init__(
            self.user.new, self.user.name, 'u/'+self.user.name, show_post_func
        )
        self.add_heading()

    def add_heading(self, *args):
        heading = UserHeading(self.user, self.refresh)
        heading_lbox_row = Gtk.ListBoxRow()
        heading_lbox_row.add(heading)
        heading_lbox_row.get_style_context().add_class(
            'non-highlighted-listbox-row'
        )
        self.section_stack.get_children()[0].post_preview_lbox.insert(
            heading_lbox_row, 0
        )
        self.show_all()

    def refresh(self, *args):
        super().refresh(*args)
        self.add_heading()
