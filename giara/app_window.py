from giara.constants import APP_ID, APP_NAME, PROFILE
from gi.repository import Gtk, Handy
from giara.confManager import ConfManager
from giara.main_ui import MainUI
from giara.accel_manager import add_accelerators


class AppWindow(Handy.ApplicationWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.confman = ConfManager()
        self.reddit = self.confman.reddit

        self.set_title(APP_NAME)
        self.set_icon_name(APP_ID+'.dev' if PROFILE == 'development' else '')

        self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.main_ui = MainUI()
        self.main_box.add(self.main_ui)
        self.main_ui.set_hexpand(True)
        self.main_ui.set_vexpand(True)

        self.add(self.main_box)

        def toggle_popover(*args):
            self.main_ui.deck.left_stack.get_headerbar().menu_btn.clicked()
            # if popover.is_visible():
            #     popover.popdown()
            # else:
            #     popover.popup()

        add_accelerators(
            self,
            [
                {
                    'combo': 'F10',
                    'cb': toggle_popover
                }
            ]
        )

        # Why this -52?
        # because every time a new value is saved, for some reason
        # it's the actual value +52 out of nowhere
        # this makes the window ACTUALLY preserve its old size
        self.resize(
            self.confman.conf['windowsize']['width']-52,
            self.confman.conf['windowsize']['height']-52
        )
        self.size_allocation = self.get_allocation()
        self.connect('size-allocate', self.update_size_allocation)

        def on_dark_mode_changed(*args):
            Gtk.Settings.get_default().set_property(
                'gtk-application-prefer-dark-theme',
                self.confman.conf['dark_mode']
            )

        self.confman.connect('dark_mode_changed', on_dark_mode_changed)
        on_dark_mode_changed()

    def emit_destroy(self, *args):
        self.emit('destroy')

    def on_destroy(self, *args):
        self.confman.conf['windowsize'] = {
            'width': self.size_allocation.width,
            'height': self.size_allocation.height
        }
        self.hide()
        self.confman.save_conf()

    def update_size_allocation(self, *args):
        self.size_allocation = self.get_allocation()

    def do_startup(self):
        pass
        # self.main_ui.source_buffer.set_language(
        #     self.main_ui.source_lang_markdown
        # )
