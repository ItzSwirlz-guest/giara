from giara.constants import RESOURCE_PREFIX
from gettext import gettext as _, ngettext
from gi.repository import Gtk, Gio
from giara.new_post_window import NewPostWindow
from giara.download_manager import download_img
from giara.squeezing_viewswitcher_headerbar import \
    SqueezingViewSwitcherHeaderbar
from giara.simple_avatar import SimpleAvatar


class FrontPageHeaderbar(SqueezingViewSwitcherHeaderbar):
    def __init__(self, front_page_stack, reddit, **kwargs):
        super().__init__(
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/headerbar.ui'
            ),
            front_page_stack,
            view_switcher=False,
            sort_menu=True,
            sorting_methods={
                'best': {
                    'name': _('Best'),
                    'icon': 'best-symbolic'
                },
                'hot': {
                    'name': _('Hot'),
                    'icon': 'hot-symbolic'
                },
                'new': {
                    'name': _('New'),
                    'icon': 'new-symbolic'
                },
                'top': {
                    'name': _('Top'),
                    'icon': 'arrow1-up-symbolic'
                },
                'rising': {
                    'name': _('Rising'),
                    'icon': 'rising-symbolic'
                },
                'controversial': {
                    'name': _('Controversial'),
                    'icon': 'controversial-symbolic'
                }
            },
            default_method='from_conf',
            **kwargs
        )
        # self.reddit = reddit  # TODO: remove this
        self.builder.connect_signals(self)

        self.menu_btn = self.builder.get_object(
            'menu_btn'
        )
        self.menu_popover = self.builder.get_object('menu_popover')
        self.menu_builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/menu.ui'
        )
        self.menu = self.menu_builder.get_object('generalMenu')
        self.menu_popover.bind_model(self.menu)

        self.new_post_action_group = Gio.SimpleActionGroup()
        for a in ('link', 'text', 'media'):
            c_action = Gio.SimpleAction.new(a, None)
            c_action.connect(
                'activate', self.on_new_clicked
            )
            self.new_post_action_group.add_action(c_action)
        self.insert_action_group('newpost', self.new_post_action_group)

        self.new_btn = self.builder.get_object('new_btn')
        self.new_post_popover = self.builder.get_object('new_post_popover')
        self.new_post_menu_builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/new_post_menu.ui'
        )
        self.new_post_menu = self.new_post_menu_builder.get_object(
            'newPostMenu'
        )
        self.new_post_popover.bind_model(self.new_post_menu)

        self.profile_btn = self.builder.get_object('profile_btn')
        self.profile_popover = self.builder.get_object('profile_popover')
        self.username_label = self.builder.get_object('username_label')
        self.karma_label = self.builder.get_object('karma_label')
        self.user = self.reddit.user.me()
        self.username_label.set_text(f'u/{self.user.name}')
        self.karma_label.set_text(ngettext(
            '{0} Karma', '{0} Karma', self.user.total_karma
        ).format(self.user.total_karma))
        self.avatar_container = self.builder.get_object('avatar_container')
        self.avatar = SimpleAvatar(
            42, self.user.name, lambda *args: download_img(self.user.icon_img)
        )
        self.avatar_container.add(self.avatar)
        self.avatar_container.show_all()
        self.profile_btn.connect(
                'clicked',
                lambda *args: self.profile_popover.popup()
        )
        self.go_subreddits_btn = self.builder.get_object('go_subreddits_btn')
        self.go_subreddits_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )
        self.go_saved_btn = self.builder.get_object('go_saved_btn')
        self.go_saved_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )

        self.go_profile_btn = self.builder.get_object('go_profile_btn')
        self.go_profile_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )

        self.go_inbox_btn = self.builder.get_object('go_inbox_btn')
        self.go_inbox_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )

        self.go_logout_btn = self.builder.get_object('go_logout_btn')
        self.go_logout_btn.connect(
            'clicked',
            self.on_logout
        )

        self.go_multireddits_btn = self.builder.get_object(
            'go_multireddits_btn'
        )
        self.go_multireddits_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )

        self.inbox_badge = self.builder.get_object('inbox_count_badge_label')
        self.confman.connect(
            'notif_count_change',
            self.on_inbox_count_change
        )

        self.refresh_btn = self.builder.get_object('refresh_btn')
        self.search_btn = self.builder.get_object('search_btn')

    def on_logout(self, *args):
        self.profile_popover.popdown()
        dialog = Gtk.MessageDialog(
            self.get_toplevel(),
            Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
            Gtk.MessageType.QUESTION,
            Gtk.ButtonsType.YES_NO,
            _('Do you want to log out? This will close the application.')
        )
        res = dialog.run()
        dialog.close()
        if res == Gtk.ResponseType.YES:
            self.confman.conf['refresh_token'] = ''
            self.confman.save_conf()
            self.get_toplevel().destroy()

    def on_inbox_count_change(self, caller, count):
        if int(count) <= 0:
            self.inbox_badge.set_text('')
            self.inbox_badge.set_visible(False)
            self.inbox_badge.set_no_show_all(True)
        else:
            self.inbox_badge.set_text(count)
            self.inbox_badge.set_visible(True)
            self.inbox_badge.set_no_show_all(False)
            self.inbox_badge.show()

    def on_menu_btn_clicked(self, *args):
        self.menu_popover.popup()

    def on_new_clicked(self, action, param):
        self.new_post_popover.popdown()
        np_win = NewPostWindow(self.reddit, action.get_name())
        np_win.set_transient_for(self.get_toplevel())
        np_win.present()
